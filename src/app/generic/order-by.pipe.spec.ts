import { TestBed } from '@angular/core/testing';
import { TransactionsService } from '../shared/transactions.service';
import { OrderByPipe } from './order-by.pipe';

describe('OrderByPipe', () => {
  let dataFixture = null;

  beforeEach(() => {
    const integers = [9, 8, 7, 6, 5, 4, 3, 2, 1];
    const strings = ['AA', 'BB', 'CC', 'DD', 'EE', 'FF', 'GG', 'HH', 'II'];
    const dates = [
      new Date('2020/09/22'),
      new Date('2020/09/20'),
      new Date('2020/09/18'),
      new Date('2020/09/16'),
      new Date('2020/09/14'),
      new Date('2020/09/15'),
      new Date('2020/09/17'),
      new Date('2020/09/19'),
      new Date('2020/09/21'),
    ]
    dataFixture = integers.map((int:number, idx:number) => ({int: int, str: strings[idx], date: dates[idx]}));
  });

  it('create an instance', () => {
    const pipe = new OrderByPipe();
    expect(pipe).toBeTruthy();
  });

  it ('should order by date ascending', () => {
    const pipe = new OrderByPipe();
    const ordered = pipe.transform(dataFixture, 'date');
    expect(ordered.slice().shift().date).toEqual(new Date('2020/09/14'));
    expect(ordered.slice(-1).shift().date).toEqual(new Date('2020/09/22'));
  });

  it ('should order by date descending', () => {
    const pipe = new OrderByPipe();
    const ordered = pipe.transform(dataFixture, '-date');
    expect(ordered.slice().shift().date).toEqual(new Date('2020/09/22'));
    expect(ordered.slice(-1).shift().date).toEqual(new Date('2020/09/14'));
  });

  it ('should order by number ascending', () => {
    const pipe = new OrderByPipe();
    const ordered = pipe.transform(dataFixture, 'int');
    expect(ordered.slice().shift().int).toEqual(1);
    expect(ordered.slice(-1).shift().int).toEqual(9);
  });

  it ('should order by int descending', () => {
    const pipe = new OrderByPipe();
    const ordered = pipe.transform(dataFixture, '-int');
    expect(ordered.slice().shift().int).toEqual(9);
    expect(ordered.slice(-1).shift().int).toEqual(1);
  });

  it ('should order by string ascending', () => {
    const pipe = new OrderByPipe();
    const ordered = pipe.transform(dataFixture, 'str');
    expect(ordered.slice().shift().str).toEqual('AA');
    expect(ordered.slice(-1).shift().str).toEqual('II');
  });

  it ('should order by string descending', () => {
    const pipe = new OrderByPipe();
    const ordered = pipe.transform(dataFixture, '-str');
    expect(ordered.slice().shift().str).toEqual('II');
    expect(ordered.slice(-1).shift().str).toEqual('AA');
  });

  it ('should return null when null array passed in', () => {
    const pipe = new OrderByPipe();
    expect(pipe.transform(null, '-str')).toEqual(null);
  });

  it ('should return copy of array when no field', () => {
    const pipe = new OrderByPipe();
    expect(pipe.transform(dataFixture, null)).toEqual(dataFixture);
  });
});
