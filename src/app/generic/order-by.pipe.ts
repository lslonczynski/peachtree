import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'orderBy'
})
export class OrderByPipe implements PipeTransform {
  transform(elems: Array<any>, field: string): Array<any> {
    if(elems==null || elems.length === 0) {
      return null;
    }
    if (!field) {
      return [...elems];
    }
    if (field.startsWith("-")) {
      field = field.substring(1);
      if (typeof elems[0][field] === 'string' || elems[0][field] instanceof String) {
        return [...elems].sort((a, b) => b[field].localeCompare(a[field]));
      }
      return [...elems].sort((a, b) => b[field] - a[field]);
    }
    else {
      if (typeof elems[0][field] === 'string' || elems[0][field] instanceof String) {
        return [...elems].sort((a, b) => -b[field].localeCompare(a[field]));
      }
      return [...elems].sort((a, b) => a[field] - b[field]);
    }
  }
}
