import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { ModalComponent } from './modal.component';

describe('ModalComponent', () => {
  let component: ModalComponent;
  let fixture: ComponentFixture<ModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it ('should emit onCancel on cancel', () => {
    spyOn(component.onCancel, 'emit');
    fixture.debugElement.query(By.css('.cancel')).triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(component.onCancel.emit).toHaveBeenCalled();
  });

  it ('should emit onConfirm on confirm', () => {
    spyOn(component.onConfirm, 'emit');
    fixture.debugElement.query(By.css('.confirm')).triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(component.onConfirm.emit).toHaveBeenCalled();
  });
});
