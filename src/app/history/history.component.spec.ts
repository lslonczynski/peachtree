import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatePipe, DecimalPipe, CurrencyPipe } from '@angular/common';

import { OrderByPipe } from '../generic/order-by.pipe';

import { HistoryComponent } from './history.component';
import { TransactionFilterPipe } from './transaction-filter.pipe';
import { TransactionsService } from '../shared/transactions.service';
import { HistorySortComponent } from './history-sort/history-sort.component';
import { HistoryFilterComponent } from './history-filter/history-filter.component';
import { HistoryItemComponent } from './history-item/history-item.component';
import { HistorySortService, SortState } from './history-sort.service';

describe('HistoryComponent', () => {
  let component: HistoryComponent;
  let fixture: ComponentFixture<HistoryComponent>;
  let transactionsService: TransactionsService;
  let historySortService: HistorySortService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        HistoryComponent,
        HistorySortComponent,
        HistoryFilterComponent,
        HistoryItemComponent,
        OrderByPipe,
        TransactionFilterPipe,
        DatePipe,
        CurrencyPipe,
        DecimalPipe,
      ],
      providers: [
        OrderByPipe,
        TransactionFilterPipe,
        DatePipe,
        CurrencyPipe,
        TransactionsService,
        DecimalPipe,
      ]
    }).compileComponents();
    historySortService = TestBed.inject(HistorySortService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoryComponent);
    component = fixture.componentInstance;
    transactionsService = TestBed.inject(TransactionsService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should invoke sort service when sort called', (done: DoneFn) => {
    fixture = TestBed.createComponent(HistoryComponent);
    component = fixture.componentInstance;
    component.sort('date');
    historySortService.state.subscribe((state: SortState) => {
      expect(state.property).toEqual('date');
      done();
    })
  });

  it('filterChanged should set searchText', () => {
    fixture = TestBed.createComponent(HistoryComponent);
    component = fixture.componentInstance;
    component.filterChanged('Test Filter');
    expect(component.searchText).toEqual('Test Filter');
  });
});
