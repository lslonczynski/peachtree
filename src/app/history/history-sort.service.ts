import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

export interface SortState {
  property: string;
  ascending: boolean;
}

@Injectable({
  providedIn: 'root',
})
export class HistorySortService {
  state = new BehaviorSubject<SortState>({ property: null, ascending: false });

  sort(key: string) {
    let {ascending, property} = this.state.value;

    if (property === key) {
      ascending = !ascending;
    } else {
      property = key;
    }

    this.state.next({
      ...this.state.value,
      property: property,
      ascending: ascending,
    });
  }

  constructor() {}
}
