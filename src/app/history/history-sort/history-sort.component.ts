import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { KeyValue } from '@angular/common';

@Component({
  selector: 'app-history-sort',
  templateUrl: './history-sort.component.html',
  styleUrls: ['./history-sort.component.scss']
})
export class HistorySortComponent implements OnInit {
  @Input() propertiesMap: object;
  @Input() active: string;
  @Input() ascending: boolean;
  @Output() onSelect = new EventEmitter<string>()

  constructor() { }

  selectProperty(property) {
    this.onSelect.emit(property.key);
  }

  sortItemsOrder = (a: KeyValue<number,string>, b: KeyValue<number,string>): number => {
    return 0;
  }

  ngOnInit(): void {
  }
}
