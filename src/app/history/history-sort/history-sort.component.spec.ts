import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { HistorySortComponent } from './history-sort.component';

describe('HistorySortComponent', () => {
  let component: HistorySortComponent;
  let fixture: ComponentFixture<HistorySortComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistorySortComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistorySortComponent);
    component = fixture.componentInstance;
    component.propertiesMap = {
      transactionDate: 'date',
      merchant: 'beneficiary',
      amount: 'amount'
    };
    component.active='transactionDate';
    component.ascending=false;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should setup according to inputs', () => {
    const transactionDate = fixture.debugElement.query(By.css('.sort-item:nth-child(2)'));
    expect(transactionDate.classes['active']).toBeTrue();
    expect(transactionDate.classes['ascending']).toBeUndefined();
  });

  it('should setup according to inputs ascending', () => {
    component.active='merchant';
    component.ascending=true;
    fixture.detectChanges();
    const merchant = fixture.debugElement.query(By.css('.sort-item:nth-child(3)'));
    expect(merchant.classes['active']).toBeTrue();
    expect(merchant.classes['ascending']).toBeTrue();
  });

  it('expect onSelect to be invoked on click', () => {
    const transactionDate = fixture.debugElement.query(By.css('.sort-item:nth-child(3)'));
    spyOn(component.onSelect, 'emit');
    transactionDate.triggerEventHandler('click', null);
    fixture.detectChanges();
    expect(component.onSelect.emit).toHaveBeenCalledWith('merchant');
  });
});
