import { DatePipe, DecimalPipe, CurrencyPipe } from '@angular/common';
import { DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { Transaction } from 'src/app/shared/transaction.model';

import { HistoryItemComponent } from './history-item.component';

describe('HistoryItemComponent', () => {
  let component: HistoryItemComponent;
  let fixture: ComponentFixture<HistoryItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HistoryItemComponent, CurrencyPipe, DecimalPipe, DatePipe],
      providers: [CurrencyPipe, DecimalPipe, DatePipe],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoryItemComponent);
    component = fixture.componentInstance;
    component.transaction = new Transaction(
      -1000,
      '#ff0000',
      'Test Merchant',
      'image-stream',
      new Date('2020/09/14'),
      'Test Type'
    );
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should contain category-box class', () => {
    expect(fixture.debugElement.query(By.css('.category-box'))).toBeTruthy();
  });

  it('category box should have background set to category code', () => {
    let categoryBox: DebugElement = fixture.debugElement.query(
      By.css('.category-box')
    );
    expect(categoryBox.styles.background).toEqual('rgb(255, 0, 0)');
  });

  it('should contain merchant', () => {
    expect(
      fixture.nativeElement.querySelector('.merchant').textContent
    ).toContain('Test Merchant');
  });

  it('should contain transaction type', () => {
    expect(
      fixture.nativeElement.querySelector('.transaction-type').textContent
    ).toContain('Test Type');
  });

  it('should contain amount', () => {
    expect(fixture.nativeElement.querySelector('.amount').textContent).toContain(
      '-$1,000.00'
    );
  });

  it('should contain image', () => {
    expect(fixture.nativeElement.querySelector('img').src).toContain(
      'image-stream'
    );
  });

  it('should contain date', () => {
    expect(fixture.nativeElement.querySelector('.date').textContent).toContain(
      'Sep. 14'
    );
  });
});
