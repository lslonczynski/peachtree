import { Component, OnInit, Input } from '@angular/core';
import { Transaction } from 'src/app/shared/transaction.model';

@Component({
  selector: 'app-history-item',
  templateUrl: './history-item.component.html',
  styleUrls: ['./history-item.component.scss']
})
export class HistoryItemComponent implements OnInit {
  @Input() transaction: Transaction;

  readonly DATE_FORMAT = Transaction.DATE_PRESENTATION_FORMAT;

  constructor() { }

  ngOnInit(): void {
  }

}
