import { Component, OnInit } from '@angular/core';

import { Subscription } from 'rxjs';

import { TransactionsService } from '../shared/transactions.service';
import { SortState, HistorySortService } from './history-sort.service';
import { Transaction } from '../shared/transaction.model';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss'],
})
export class HistoryComponent implements OnInit {
  transactions: Transaction[] = [];

  readonly sortPropertiesMap = {
    transactionDate: 'date',
    merchant: 'beneficiary',
    amount: 'amount'
  }

  activeSortProperty: string = 'transactionDate';
  activeSortAscending: boolean = true;
  orderByRecipe: string;
  searchText: string = '';
  private sortStateSub: Subscription;

  constructor(
    private TransactionsService: TransactionsService,
    private historySortService: HistorySortService
  ) {}

  ngOnInit(): void {
    this.TransactionsService.transactions.subscribe(transactions => this.transactions = transactions);

    this.historySortService.sort('transactionDate');
    this.historySortService.state.subscribe((state: SortState) => {
      this.activeSortProperty = state.property;
      this.activeSortAscending = state.ascending;
      this.orderByRecipe = (state.ascending ? '' : '-') + state.property;
    });
  }

  sort(property: string) {
    this.historySortService.sort(property);
  }

  filterChanged(searchText: string) {
    this.searchText = searchText;
  }
}
