import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe, CurrencyPipe } from '@angular/common';

import { Transaction } from '../shared/transaction.model';

@Pipe({
  name: 'transactionFilter',
})
export class TransactionFilterPipe implements PipeTransform {
  constructor(private datePipe: DatePipe, private currencyPipe: CurrencyPipe) {}

  transform(elems: Array<Transaction>, searchText: string): Array<Transaction> {
    if (!elems || elems.length === 0) {
      return null;
    }
    if (!searchText) {
      return [...elems];
    }
    const mask = searchText.toLowerCase();
    return elems.filter((transaction: Transaction) =>
      [
        this.datePipe.transform(transaction.transactionDate, Transaction.DATE_PRESENTATION_FORMAT),
        transaction.merchant,
        transaction.transactionType,
        this.currencyPipe.transform(transaction.amount)
      ]
      .map(str => str.toLowerCase())
      .some((value: string) => value.includes(mask))
    );
  }
}
