import { DebugElement } from '@angular/core';
import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { HistoryFilterComponent } from './history-filter.component';

describe('HistoryFilterComponent', () => {
  let component: HistoryFilterComponent;
  let fixture: ComponentFixture<HistoryFilterComponent>;
  let input: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoryFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoryFilterComponent);
    component = fixture.componentInstance;
    component.onChange.subscribe((text: string) => console.log('text', text));
    input = fixture.debugElement.query(By.css('input'));
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // it('should raise selected event when clicked (triggerEventHandler)', fakeAsync(() => {
  //   fixture = TestBed.createComponent(HistoryFilterComponent);
  //   tick();
  //   component = fixture.componentInstance;
  //   // component.onChange.subscribe((text: string) => console.log('text', text));
  //   fixture.detectChanges();
  //   input = fixture.debugElement.query(By.css('input'));
  //   input.nativeElement.value = 'test';
  //   input.triggerEventHandler('input', {
  //     target: input.nativeElement
  //   });
  //   expect(component.value).toEqual('test');
  // }));

  it ('should emit onChange on change', () => {
    spyOn(component.onChange, 'emit');
    component.changed('test');
    // fixture.detectChanges();
    expect(component.onChange.emit).toHaveBeenCalledWith('test');
  });
});
