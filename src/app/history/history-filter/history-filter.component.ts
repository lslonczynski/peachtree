import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-history-filter',
  templateUrl: './history-filter.component.html',
  styleUrls: ['./history-filter.component.scss']
})
export class HistoryFilterComponent implements OnInit {
  @Output() onChange = new EventEmitter<string>();

  constructor() {}

  ngOnInit(): void {
  }

  changed(text: string) {
    this.onChange.emit(text);
  }

}
