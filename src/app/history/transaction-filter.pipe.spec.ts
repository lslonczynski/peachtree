import { DatePipe, DecimalPipe, CurrencyPipe } from '@angular/common';
import { TestBed } from '@angular/core/testing';
import { Transaction } from '../shared/transaction.model';
import { TransactionsService } from '../shared/transactions.service';
import { TransactionFilterPipe } from './transaction-filter.pipe';

describe('TransactionFilterPipe', () => {
  let datePipe: DatePipe;
  let currencyPipe: CurrencyPipe;
  let transactionsService: TransactionsService;

  beforeEach(() => {
    TestBed
      .configureTestingModule({
        providers: [
          DatePipe,
          DecimalPipe,
          CurrencyPipe,
        ]
      });
    datePipe = TestBed.inject(DatePipe);
    currencyPipe = TestBed.inject(CurrencyPipe);
    transactionsService = TestBed.inject(TransactionsService);
  });

  it('create an instance', () => {
    const pipe = new TransactionFilterPipe(datePipe, currencyPipe);
    expect(pipe).toBeTruthy();
  });

  it('should filter by date in display format', (done: DoneFn) => {
    const pipe = new TransactionFilterPipe(datePipe, currencyPipe);
    transactionsService.transactions.subscribe((transactions: Transaction[]) => {
      expect(pipe.transform(transactions, 'Oct. 19').length).toEqual(2);
      done();
    });
  });

  it('should filter by part of the date in display format', (done: DoneFn) => {
    const pipe = new TransactionFilterPipe(datePipe, currencyPipe);
    transactionsService.transactions.subscribe((transactions: Transaction[]) => {
      expect(pipe.transform(transactions, 't. 19').length).toEqual(2);
      done();
    });
  });

  it('should filter by merchant', (done: DoneFn) => {
    const pipe = new TransactionFilterPipe(datePipe, currencyPipe);
    transactionsService.transactions.subscribe((transactions: Transaction[]) => {
      const matchingTransactions = pipe.transform(transactions, 'Tea Lounge');
      expect(matchingTransactions.length).toEqual(2);
      expect(matchingTransactions.filter((transaction: Transaction) => transaction.merchant === 'The Tea Lounge').length).toEqual(2);
      done();
    });
  });

  it('should filter when multiple merchants match', (done: DoneFn) => {
    const pipe = new TransactionFilterPipe(datePipe, currencyPipe);
    transactionsService.transactions.subscribe((transactions: Transaction[]) => {
      const matchingTransactions = pipe.transform(transactions, 'Te');
      expect(matchingTransactions.length).toEqual(3);
      expect(matchingTransactions.filter((transaction: Transaction) => transaction.merchant.includes('Te')).length).toEqual(3);
      done();
    });
  });

  it('should filter by transactionType', (done: DoneFn) => {
    const pipe = new TransactionFilterPipe(datePipe, currencyPipe);
    transactionsService.transactions.subscribe((transactions: Transaction[]) => {
      const matchingTransactions = pipe.transform(transactions, 'Online Transfer');
      expect(matchingTransactions.length).toEqual(3);
      expect(matchingTransactions.filter((transaction: Transaction) => transaction.transactionType.includes('Online Transfer')).length).toEqual(3);
      done();
    });
  });

  it('should filter by part of transactionType', (done: DoneFn) => {
    const pipe = new TransactionFilterPipe(datePipe, currencyPipe);
    transactionsService.transactions.subscribe((transactions: Transaction[]) => {
      const matchingTransactions = pipe.transform(transactions, 'Online');
      expect(matchingTransactions.length).toEqual(3);
      expect(matchingTransactions.filter((transaction: Transaction) => transaction.transactionType.includes('Online')).length).toEqual(3);
      done();
    });
  });

  it('should filter by amount', (done: DoneFn) => {
    const pipe = new TransactionFilterPipe(datePipe, currencyPipe);
    transactionsService.transactions.subscribe((transactions: Transaction[]) => {
      const matchingTransactions = pipe.transform(transactions, '84.64');
      expect(matchingTransactions.length).toEqual(1);
      expect(matchingTransactions[0].amount).toEqual(-84.64);
      done();
    });
  });

  it('should filter by amount w $ sign', (done: DoneFn) => {
    const pipe = new TransactionFilterPipe(datePipe, currencyPipe);
    transactionsService.transactions.subscribe((transactions: Transaction[]) => {
      const matchingTransactions = pipe.transform(transactions, '$84.64');
      expect(matchingTransactions.length).toEqual(1);
      expect(matchingTransactions[0].amount).toEqual(-84.64);
      done();
    });
  });

  it('should filter by amount w minus and $ sign', (done: DoneFn) => {
    const pipe = new TransactionFilterPipe(datePipe, currencyPipe);
    transactionsService.transactions.subscribe((transactions: Transaction[]) => {
      const matchingTransactions = pipe.transform(transactions, '-$84.64');
      expect(matchingTransactions.length).toEqual(1);
      expect(matchingTransactions[0].amount).toEqual(-84.64);
      done();
    });
  });

  it('should filter by amount w partial amount', (done: DoneFn) => {
    const pipe = new TransactionFilterPipe(datePipe, currencyPipe);
    transactionsService.transactions.subscribe((transactions: Transaction[]) => {
      const matchingTransactions = pipe.transform(transactions, '84.');
      expect(matchingTransactions.length).toEqual(2);
      expect(matchingTransactions.filter((transaction: Transaction) => transaction.amount <= -84 && transaction.amount > -85).length).toEqual(2);
      done();
    });
  });

  it('should receive null when null is passed', () => {
    const pipe = new TransactionFilterPipe(datePipe, currencyPipe);
    expect(pipe.transform(null, '84.')).toEqual(null);
    expect(pipe.transform([], '84.')).toEqual(null);
  });
});
