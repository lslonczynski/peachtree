import { TestBed } from '@angular/core/testing';

import { HistorySortService, SortState } from './history-sort.service';

describe('HistorySortService', () => {
  let service: HistorySortService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HistorySortService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it ('first sort is descending', (done: DoneFn) => {
    service.sort('property-key');
    service.state.subscribe((state: SortState) => {
      expect(state.property).toEqual('property-key');
      expect(state.ascending).toBeFalse();
	    done();
    });
  });

  it ('sort with same property switches the order', (done: DoneFn) => {
    service.sort('property-key');
    service.sort('property-key');
    service.state.subscribe((state: SortState) => {
      expect(state.property).toEqual('property-key');
      expect(state.ascending).toBeTrue();
	    done();
    });
  });

  it ('sort with same property preserves the order (desc)', (done: DoneFn) => {
    service.sort('property-key');
    service.sort('property2-key');
    service.state.subscribe((state: SortState) => {
      expect(state.property).toEqual('property2-key');
      expect(state.ascending).toBeFalse();
	    done();
    });
  });

  it ('sort with same property preserves the order (asc)', (done: DoneFn) => {
    service.sort('property-key');
    service.sort('property-key');
    service.sort('property2-key');
    service.state.subscribe((state: SortState) => {
      expect(state.property).toEqual('property2-key');
      expect(state.ascending).toBeTrue();
	    done();
    });
  });
});
