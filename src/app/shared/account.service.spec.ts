import { TestBed } from '@angular/core/testing';

import { AccountService } from './account.service';
import { TransactionsService } from './transactions.service';
import { Account } from './account.model';

describe('AccountService', () => {
  let service: AccountService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        TransactionsService
      ]
    });
    service = TestBed.inject(AccountService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should initialize with correct data', (done: DoneFn) => {
    service.activeAccount.subscribe((account: Account) => {
      expect(account.name).toEqual('Free Checking(4692)');
      expect(account.debitLimit).toEqual(500);
      expect(account.balance).toEqual(5824.26);
      done();
    });
  });

  it('should deduct amount used in transaction', (done: DoneFn) => {
    let transactionService = TestBed.inject(TransactionsService);
    transactionService.registerOnlineTransfer('Test Recipient', -1000);
    service.activeAccount.subscribe((account: Account) => {
      expect(account.balance).toEqual(4824.26);
      done();
    });
  });

  it('should sum up amounts from multiple transactions', (done: DoneFn) => {
    let transactionService = TestBed.inject(TransactionsService);
    transactionService.registerOnlineTransfer('Test Recipient', -1000);
    transactionService.registerOnlineTransfer('Test Recipient', -500);
    transactionService.registerOnlineTransfer('Test Recipient', -250);
    service.activeAccount.subscribe((account: Account) => {
      expect(account.balance).toEqual(4074.26);
      done();
    });
  });
});
