export class Account {
  constructor(public name: string, public balance: number, public debitLimit: number) {}
}
