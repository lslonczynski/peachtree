import { TestBed } from '@angular/core/testing';
import { Transaction } from './transaction.model';

import { TransactionsService } from './transactions.service';

describe('TransactionsService', () => {
  let service: TransactionsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TransactionsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should initialize with transactions', (done: DoneFn) => {
    service.transactions.subscribe((transactions: Transaction[]) => {
      expect(transactions.length).toEqual(10);
      done();
    });
  });

  it('adding online transfer should add transaction to the collection', (done: DoneFn) => {
    service.registerOnlineTransfer('Test Recipient', 1000);
    service.transactions.subscribe((transactions: Transaction[]) => {
      expect(transactions.length).toEqual(11);
      expect(transactions.filter((transaction: Transaction) => transaction.merchant === 'Test Recipient').length).toEqual(1);
      done();
    });
  });

  it('similar transactions should be treated as separate entities', (done: DoneFn) => {
    service.registerOnlineTransfer('Test Recipient', 1000);
    service.registerOnlineTransfer('Test Recipient', 1000);
    service.registerOnlineTransfer('Test Recipient', 1000);
    service.transactions.subscribe((transactions: Transaction[]) => {
      expect(transactions.length).toEqual(13);
      expect(transactions.filter((transaction: Transaction) => transaction.merchant === 'Test Recipient').length).toEqual(3);
      done();
    });
  });
});
