import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { Account } from './account.model';
import { Transaction } from './transaction.model';
import { TransactionsService } from './transactions.service';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  private readonly initialBalance = 679.60 + 5824.26;
  public activeAccount = new BehaviorSubject<Account>(new Account('Free Checking(4692)', this.initialBalance, 500));

  constructor(private transactionsService: TransactionsService) {
    this.transactionsService.transactions.subscribe(transactions => this.activeAccount.next(
      {
        ...this.activeAccount.value,
        balance: this.initialBalance + transactions
        .map((elem: Transaction) => elem.amount)
        .reduce((sum, elem) => sum + elem)
      }
    ));
  }
}
