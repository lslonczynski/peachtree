import { Injectable } from '@angular/core';
import { Transaction } from './transaction.model';
import transactionsJson from '../history/transactions.json';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class TransactionsService {
  transactions = new BehaviorSubject<Transaction[]>([]);
  constructor() {
    this.transactions.next(
      transactionsJson.data.map(
        (transactionItem) =>
          new Transaction(
            -transactionItem.amount,
            transactionItem.categoryCode,
            transactionItem.merchant,
            transactionItem.merchantLogo,
            new Date(transactionItem.transactionDate),
            transactionItem.transactionType
          )
      )
    );
  }

  registerOnlineTransfer(toAccount: string, amount: number) {
    this.transactions.next(
      [
        ...this.transactions.value,
        new Transaction(
          amount,
          '#be5b43',
          toAccount,
          "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIEAAACBCAYAAADnoNlQAAABhGlDQ1BJQ0MgcHJvZmlsZQAAKJF9kT1Iw0AcxV9TxQ9aFMwg0iFDdbIgKuIoVSyChdJWaNXB5NIvaNKQpLg4Cq4FBz8Wqw4uzro6uAqC4AeIk6OToouU+L+k0CLWg+N+vLv3uHsHCPUy06yuCUDTbTMZi0qZ7KrU84o+BCFiECGZWUY8tZhGx/F1Dx9f7yI8q/O5P0dQzVkM8EnEc8wwbeIN4plN2+C8TyyyoqwSnxOPm3RB4keuKx6/cS64LPBM0Uwn54lFYqnQxkobs6KpEU8Th1VNp3wh47HKeYuzVq6y5j35CwM5fSXFdZohxLCEOBKQoKCKEsqwEaFVJ8VCkvajHfwjrj9BLoVcJTByLKACDbLrB/+D391a+alJLykQBbpfHOdjFOjZBRo1x/k+dpzGCeB/Bq70lr9SB2Y/Sa+1tPARMLANXFy3NGUPuNwBhp8M2ZRdyU9TyOeB9zP6piwwdAv0r3m9Nfdx+gCkqavlG+DgEBgrUPZ6h3f3tvf275lmfz9hNHKgvemZogAAAAZiS0dEAP8A/wD/oL2nkwAAAAlwSFlzAAAuIwAALiMBeKU/dgAAAAd0SU1FB+QJDRc0N6SFqpUAAAAZdEVYdENvbW1lbnQAQ3JlYXRlZCB3aXRoIEdJTVBXgQ4XAAADIElEQVR42u3dMW4TQRiG4c+vfJQUpAtXwByASBRIpjA1FQVViggBQumSCrpQhJqUNPgK0NH4ACkpKShoHESViRhrZzfzfo0trWRr1v8881vanU06znq5mK+Xi4fpPHQ+/qdJ3q+Xi7lF0KkCSY6S7G2LwSLoVIG97fujnjWgcwWu07UGqMDfdKsBKqAGqIAaoAJqgAqoASqgBqiAGqACaoAKqAEqoAaogBqgAmqACqgBKqAGqIAaoAJqgAqoASqgBqiAGqACaoAKqAEqoAaogBqgAmqACqgBKqAGqIAaoAJqgAqoASqgBqiAGqACaoAKqAEqoAaogBqgAmqACqgBKqAGqIAaoAJqgAqoASqgBqiAGqACaoAKqAEqoAaogBqgAmqACqgBKqAGqIAaoAJqgAqoASqgBqiAGqACaoAKqAEqoAaogBrMJ67ArySH29dWeZ7kcaUGH1v+DrPGRbBKcl7xEadJXoyA9R8VE2qTZP/Bp6+/u1sOdqTAyQhI3yS5mHJv0LInqO0FPiS5Gsna/jZJzUxu2hugAmqACqgBKqAGqIAaoAJqgAqoASqgBqiAGqACaoAKqAEqoAaogBowcgV+JnmV6WYSGjByBc62hTDljF4DRq7Aaaaf0WuACqgBKqAGqIAaoAJqgAqoASqgBqiAGqACaoAKqAEqoAaogBqgAmqACqjBvLECmyRvCtVeuu37Km33Jyh9/0mSLzeM/zKN9zeYVRbBKnX7CzwrDOA8yWris/17kvs3HD9I8q1yIlXtb0BjBS6G/D/cKAdJHhWK5LJlb1DTE9T2AqX18CiNt9PZYY4Lx2svoavqDVABNUAF1AAVUANUQA1QATVABdSAESlw744r8K8GT8akASNS4F0HClzndWGsg2rASBQorZV3LaXZOqgGDKTAy4ICx+kvpf6ndM52pgEDKFCq6t4UuO1sHex6AwZQoLS+9ajAbTUY5HoDVEANUAE1QAXUABVQA1RADVABNUAF1AAVUANUQA1QATVABdQAFVADVEANUAE1QAXUYLaDO4sPC0Xw2SL472yS7N8w43fyxHZPs8kfjWwRGTYi6i0AAAAASUVORK5CYII=",
          new Date(),
          'Online Transfer'
        )
      ]
    )
  }
}
