export class Transaction {
  constructor(
    public amount: number,
    public categoryCode: string,
    public merchant: string,
    public merchantLogo: string,
    public transactionDate: Date,
    public transactionType: string
  ) {}

  static readonly DATE_PRESENTATION_FORMAT = "MMM. d";
}
