import { ReactiveFormsModule } from '@angular/forms';
import { DecimalPipe } from '@angular/common';
import { SimpleChange } from '@angular/core';
import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { Account } from '../shared/account.model';
import { AccountService } from '../shared/account.service';

import { TransferComponent } from './transfer.component';

describe('TransferComponent', () => {
  let component: TransferComponent;
  let fixture: ComponentFixture<TransferComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransferComponent, DecimalPipe ],
      providers: [DecimalPipe, AccountService],
      imports: [ReactiveFormsModule],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransferComponent);
    component = fixture.componentInstance;
    component.account = new Account('Account Name', 1000, 500);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should react to amount change in the form', () => {
    component.transferForm.patchValue({amount: '100'});
    fixture.detectChanges();
    expect(component.transferForm.get('amount').value).toEqual('100.00');
  });

  it ('should update fromAccount on external amount change', () => {
    component.ngOnChanges({account: new SimpleChange(component.account, new Account('Account Name', 500, 500), false)});
    expect(component.transferForm.get('fromAccount').value).toEqual('Account Name - 500.00');
  });

  it('should open confirmation modal on submitForm', () => {
    component.submitForm();
    expect(component.showConfirmationRequest).toBeTrue();
  });

  it('should close confirmation modal on cancelTrasfer', () => {
    component.submitForm();
    expect(component.showConfirmationRequest).toBeTrue();
    component.cancelTransfer();
    expect(component.showConfirmationRequest).toBeFalse();
  });

  it('should emit onTransfer modal on processTrasfer', () => {
    expect(component.showConfirmationRequest).toBeFalse();
    spyOn(component.onTransfer, 'emit');
    component.transferForm.patchValue({amount: '100', toAccount: 'Test Recipient'});

    component.processTransfer();
    fixture.detectChanges();
    expect(component.onTransfer.emit).toHaveBeenCalledWith({toAccount: 'Test Recipient', amount: -100});
  });

  it('trigger key on element', fakeAsync(() => {
    const keypress = new KeyboardEvent('keypress', {
      key: '1',
      cancelable: true
    });
    const amountElem = fixture.debugElement.query(By.css('.money-amount'));
    amountElem.triggerEventHandler('keydown', keypress);
    fixture.detectChanges();
    expect(true).toEqual(true); //this test case is just to run the test case
  }));
});
