
import {
  TransferHelper,
  SimplifiedKeyDownInfo,
  ProcessKeyResult,
} from './transfer.helper';

describe('TransactionFilterPipe', () => {
  it('shouldnt block digits', () => {
    expect(TransferHelper.processKeyDown(new SimplifiedKeyDownInfo('5', false), '100.00', 0, 0).blockEvent).toBeFalse();
  });

  it('should block letters', () => {
    expect(TransferHelper.processKeyDown(new SimplifiedKeyDownInfo('a', false), '100.00', 0, 0).blockEvent).toBeTrue();
  });

  it('should block operators', () => {
    expect(TransferHelper.processKeyDown(new SimplifiedKeyDownInfo('a', false), '100.00', 0, 0).blockEvent).toBeTrue();
  });

  it('should block commas and colons when not at separator', () => {
    const result = TransferHelper.processKeyDown(new SimplifiedKeyDownInfo(',', false), '100.00', 1, 1);
    expect(result.blockEvent).toBeTrue();
    expect(result.patchSelectionStart).toEqual(-1);
  });

  it('should allow commas and colons when when at separator', () => {
    const result = TransferHelper.processKeyDown(new SimplifiedKeyDownInfo(',', false), '100.00', 3, 3);
    expect(result.blockEvent).toBeTrue();
    expect(result.patchSelectionStart).toEqual(4);
  });

  it('should allow overwrite of decimal digit', () => {
    const result = TransferHelper.processKeyDown(new SimplifiedKeyDownInfo('1', false), '100.00', 4, 4);
    expect(result.blockEvent).toBeTrue();
    expect(result.patchAmount).toEqual('100.10');
  });
});
