import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Account } from '../shared/account.model';
import { Transaction } from '../shared/transaction.model';
import {
  TransferHelper,
  SimplifiedKeyDownInfo,
  ProcessKeyResult,
} from './transfer.helper';

export interface TransferData {
  toAccount: string;
  amount: number;
}

@Component({
  selector: 'app-transfer',
  templateUrl: './transfer.component.html',
  styleUrls: ['./transfer.component.scss'],
})
export class TransferComponent implements OnInit, OnChanges {
  @Input() account: Account;
  @Output() onTransfer = new EventEmitter<TransferData>();
  @ViewChild('amt') amountControl: ElementRef;

  readonly DATE_FORMAT = Transaction.DATE_PRESENTATION_FORMAT;

  transferForm: FormGroup;
  confirmationMessage: string = 'Please confirm transfer request';
  showConfirmationRequest = false;
  transactionDate = null;

  defaultValues = {
    fromAccount: '',
    toAccount: '',
    amount: '0.00',
  };

  private readonly defaultFormat: string = '1.2-2';

  constructor(private decimalPipe: DecimalPipe) {}

  ngOnInit(): void {
    const initialAmount: string = '0.00';
    const amountControl = new FormControl(this.defaultValues.amount, [
      Validators.required,
      Validators.pattern('[0-9]+([.,][0-9][0-9]?)?'),
      Validators.max(this.account.balance + this.account.debitLimit),
      Validators.min(0.01),
    ]);
    this.transferForm = new FormGroup({
      fromAccount: new FormControl({
        value: this.defaultValues.fromAccount,
        disabled: true,
      }),
      toAccount: new FormControl(
        this.defaultValues.toAccount,
        Validators.required
      ),
      amount: amountControl,
    });

    amountControl.valueChanges.subscribe((amount: string) => {
      if (!amount) {
        return;
      }

      const decimalSeparators = /[\.\,]/;
      const decimalSplitIndex = amount.search(decimalSeparators);

      if (decimalSplitIndex !== -1) {
        return;
      }
      if (amount !== this.decimalPipe.transform(amount, this.defaultFormat)) {
        const cursorPosition = this.amountControl.nativeElement.selectionStart;
        this.transferForm.patchValue({
          amount: this.decimalPipe.transform(amount, this.defaultFormat),
        });
        this.amountControl.nativeElement.selectionStart = this.amountControl.nativeElement.selectionEnd = cursorPosition;
      }
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    this.defaultValues.fromAccount =
      changes.account.currentValue.name +
      ' - ' +
      this.decimalPipe.transform(
        changes.account.currentValue.balance,
        this.defaultFormat
      );
    if (this.transferForm) {
      this.transferForm.patchValue({
        fromAccount: this.defaultValues.fromAccount,
      });
    }
  }

  amountKeyDown(event: KeyboardEvent) {
    const amount: string = this.transferForm.value.amount;
    const processKeyResult: ProcessKeyResult = TransferHelper.processKeyDown(
      new SimplifiedKeyDownInfo(event.key, event.ctrlKey),
      amount,
      this.amountControl.nativeElement.selectionStart,
      this.amountControl.nativeElement.selectionEnd
    );

    if (processKeyResult.patchSelectionStart !== -1) {
      this.amountControl.nativeElement.selectionStart = processKeyResult.patchSelectionStart;
    }

    if (processKeyResult.patchSelectionEnd !== -1) {
      this.amountControl.nativeElement.selectionEnd = processKeyResult.patchSelectionEnd;
    }

    if (processKeyResult.patchAmount !== null) {
      this.transferForm.patchValue({
        amount: processKeyResult.patchAmount
      });
    }

    if (processKeyResult.blockEvent) {
      event.preventDefault();
    }
  }

  submitForm() {
    this.transactionDate = new Date();
    this.showConfirmationRequest = true;
  }

  cancelTransfer() {
    this.showConfirmationRequest = false;
  }

  processTransfer() {
    const { amount, toAccount } = this.transferForm.value;
    this.showConfirmationRequest = false;
    this.onTransfer.emit({ toAccount: toAccount, amount: -amount });
    this.transferForm.reset(this.defaultValues);
  }

  transferLimit() {
    return this.account.balance + this.account.debitLimit;
  }
}
