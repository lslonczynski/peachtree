export class SimplifiedKeyDownInfo {
  constructor(public key: string, public ctrlKey: boolean) {}
}

export class ProcessKeyResult {
  constructor(public blockEvent: boolean, public patchAmount: string, public patchSelectionStart: number, public patchSelectionEnd: number) {}
}

export class TransferHelper {
  static processKeyDown(keyInfo: SimplifiedKeyDownInfo, amount: string, selectionStart: number, selectionEnd: number) {
    let patchSelectionStart = -1;
    let patchSelectionEnd = -1;
    let patchAmount = null;

    const decimalSeparators = /[\.\,]/;

    const decimalSplitIndex = amount.search(decimalSeparators);
    const separatorPressed = keyInfo.key.match(decimalSeparators);
    const cursorPosition = selectionStart;
    const selectionSize = selectionEnd - selectionStart;

    const separatorOnSeparator =
      separatorPressed &&
      cursorPosition === decimalSplitIndex &&
      selectionSize === 0; // separator was pressed with cursor at separator
    if (separatorOnSeparator) {
      patchSelectionEnd = patchSelectionStart = cursorPosition + 1;
    }

    const overwriteDecimalDigit =
      keyInfo.key.match(/^[0-9]$/) &&
      cursorPosition > decimalSplitIndex &&
      cursorPosition <= decimalSplitIndex + 2 &&
      selectionSize === 0;
    if (overwriteDecimalDigit) {
      patchAmount = amount.substr(0, cursorPosition) + keyInfo.key + amount.substr(cursorPosition + 1);
      patchSelectionEnd = patchSelectionStart = cursorPosition + 1;
    }

    const blockEvent: boolean = [
      separatorPressed && cursorPosition === 0, // separator not allowed at the beginning of the string
      separatorPressed && decimalSplitIndex !== -1, // only one decimal separator allowed
      keyInfo.key.match(/^[a-zA-Z]$/) && !keyInfo.ctrlKey, // [a-zA-Z] not allowed
      '!@#$%^&*()-_+={}[];:\'"/?><'.includes(keyInfo.key), // forbidden characters
      keyInfo.key.match(/^[0-9]$/) && cursorPosition > decimalSplitIndex + 2, // more than two decimal digits not allowed
      separatorOnSeparator, // case handled manually
      overwriteDecimalDigit, // case handled manually
    ].includes(true);

    return new ProcessKeyResult(blockEvent, patchAmount, patchSelectionStart, patchSelectionEnd);
  }
}
