import { Component } from '@angular/core';

import { TransferData } from './transfer/transfer.component';

import { Account } from './shared/account.model';
import { AccountService } from './shared/account.service';
import { TransactionsService } from './shared/transactions.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Peachtree';
  activeAccount: Account;
  constructor(
    private accountService: AccountService,
    private transactionsService: TransactionsService
  ) {
    this.accountService.activeAccount.subscribe(account => this.activeAccount = account);
  }

  processTransfer(transferData: TransferData) {
    this.transactionsService.registerOnlineTransfer(transferData.toAccount, transferData.amount);
  }
}
