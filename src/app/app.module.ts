import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { DatePipe, DecimalPipe, CurrencyPipe } from '@angular/common';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { TransferComponent } from './transfer/transfer.component';
import { HistoryComponent } from './history/history.component';
import { HistoryItemComponent } from './history/history-item/history-item.component';
import { HistorySortComponent } from './history/history-sort/history-sort.component';
import { HistoryFilterComponent } from './history/history-filter/history-filter.component';
import { OrderByPipe } from './generic/order-by.pipe';
import { TransactionFilterPipe } from './history/transaction-filter.pipe';
import { ModalComponent } from './generic/modal/modal.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    TransferComponent,
    HistoryComponent,
    HistoryItemComponent,
    HistorySortComponent,
    HistoryFilterComponent,
    OrderByPipe,
    TransactionFilterPipe,
    ModalComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    DatePipe,
    DecimalPipe,
    CurrencyPipe,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
