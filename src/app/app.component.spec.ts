
import { TestBed, async } from '@angular/core/testing';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { DatePipe, DecimalPipe, CurrencyPipe } from '@angular/common';
import { AppComponent } from './app.component';
import { OrderByPipe } from './generic/order-by.pipe';
import { HeaderComponent } from './header/header.component';
import { HistoryComponent } from './history/history.component';
import { HistoryFilterComponent } from './history/history-filter/history-filter.component';
import { HistoryItemComponent } from './history/history-item/history-item.component';
import { HistorySortComponent } from './history/history-sort/history-sort.component';
import { TransactionFilterPipe } from './history/transaction-filter.pipe';
import { TransactionsService } from './shared/transactions.service';
import { TransferComponent } from './transfer/transfer.component';
import { Transaction } from './shared/transaction.model';

describe('AppComponent', () => {
  let transactionsService: TransactionsService = null;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        FormsModule
      ],
      declarations: [
        AppComponent,
        HeaderComponent,
        HistoryComponent,
        OrderByPipe,
        TransactionFilterPipe,
        DatePipe,
        CurrencyPipe,
        DecimalPipe,
        TransferComponent,
        HistoryFilterComponent,
        HistoryItemComponent,
        HistorySortComponent,
      ],
      providers: [
        OrderByPipe,
        TransactionFilterPipe,
        DatePipe,
        CurrencyPipe,
        DecimalPipe,
        TransactionsService,
      ],
    }).compileComponents();
    transactionsService = TestBed.inject(TransactionsService);
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'Peachtree'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('Peachtree');
  });

  it('should render header', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('.app-header')).toBeTruthy();
  });

  it('should process transfer', (done: DoneFn) => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    app.processTransfer({toAccount:'Test Account', amount: 100});
    transactionsService.transactions.subscribe((transactions: Transaction[]) => {
      expect(transactions.length).toEqual(11);
      expect(transactions.filter((transaction: Transaction) => transaction.merchant === 'Test Account').length).toEqual(1);
      done();
    });
  });

});
