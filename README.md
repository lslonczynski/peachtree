#  Peachtree Project - Useful Information

## BitBucket Repository

Latest version of project sources is available at https://bitbucket.org/lslonczynski/peachtree

## Running version online

Application is running at https://peachtree-6f54e.web.app/

## Setup app

Clone repository `git clone https://bitbucket.org/lslonczynski/peachtree`

Run `npm install` from it's root directory (to get there `cd peachtree`)

## Start app

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. 

## Build app

Run `ng build` to build the project.

## Run tests

Run `ng test` to execute unit tests

Run `ng test --codeCoverage` to execute unit tests and generate coverage reports

## App Structure

### Logical Structure

Main Components:

1. AppComponent - glue for other components, facilitates processing transfer
2. TransferComponent - pure - provides form for user to input transfer data, validates inputs, requests confirmation and passes data out
3. HistoryComponent - smart - facilitates communication between its' children responsible for sorting, filtering and presenting/keeps state with sync.

Main Services:

1. TransactionsService - maintains list of current transactions. HistoryComponent subscribe to this service to pull transactions, AppComponent uses it method to push new transfers
2. AccountService - maintains information about current account, AppComponent uses it to keep in sync with account name, balance and debit limit

### Directory Structure

1. Source code is organized into component folders - each component has dedicated folder which contains its source code, template code, styles, services, pipes, directives
2. Directory structure follow components structure - nested components have nested folders.
3. If some artifact is being used by more than one component its located in first common ancestor
4. If first common ancestor is `app` folder its located in `app/shared` folder
5. If artifact is not limited to the context of this specific app (doesn't assume anything about the domain) it's put to `app/generic` folder



## Requirements Implementation Report

### Transfer Money

Scenario is supported with all additional requirements (form resetting, limit enforcing)

### Transaction History

All scenarios are supported.

For non-functional requirements: 
- The Sorting order (ascending/descending) should be persistent across all sorting options; i.e. If you are sorting by beneficiary ASC and switch the sorting option to Amount, the sorting order should stay ASC - `IMPLEMENTED`
- i18n: add multi-language support - `NOT IMPLEMENTEED` - due to time constraint
- a11y: WCAG level A - `SOMEWHAT IMPLEMENTED` - please see below
- Share your solution on the remote repository; i.e. Github, Bitbucket, GitLab... - `DONE` - https://bitbucket.org/lslonczynski/peachtree
- deploy to a static hosting platform of your choice; i.e. Netlify, Firebase, Vercel, Github pages or Heroku - `DONE` - https://peachtree-6f54e.web.app/
- decent test coverage - `DONE` - 95%+ line coverage


## Additional Information

### Accessibility
I don't know all the rules of WCAG A, and due to time constraint I am not able to read through the docs, so I don't have a way to establish how well it is covered. 

All standard gotchas are implemented though:
1. Application contains structural elements
2. Every input has its' label
3. Every meaningful media artifact has its alt description
4. Every element which can be used to invoke action will be understood by screen readers as such
5. Application is fully controllable by keyboard - every element which has action bound is on the tab path

### Resposiveness
Application should be fairly responsive down to 360px of width (or to be precise 22.5rem of width) - below that size horizontal scroller will show up. Most of the measures are expressed in rem unit so it should also react correctly to font size changes in the browsers.




